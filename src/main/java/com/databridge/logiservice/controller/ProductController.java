package com.databridge.logiservice.controller;

import com.databridge.logiservice.business.ProductBusinessInterface;
import com.databridge.logiservice.controller.common.RestController;
import com.databridge.logiservice.object.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/product")
public class ProductController extends RestController<Product, ProductBusinessInterface> {

    @Autowired
    public ProductController(ProductBusinessInterface productBusiness) {
        super(productBusiness);
    }
}

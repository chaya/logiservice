package com.databridge.logiservice.controller;

import com.databridge.logiservice.business.TiersBusinessInterface;
import com.databridge.logiservice.controller.common.RestController;
import com.databridge.logiservice.object.Tiers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/tiers")
public class TiersController extends RestController<Tiers, TiersBusinessInterface> {

    @Autowired
    public TiersController(TiersBusinessInterface tiersBusiness) {
        super(tiersBusiness);
    }
}

package com.databridge.logiservice.controller.common;

import com.databridge.logiservice.business.common.ScrudBusinessInterface;
import com.databridge.logiservice.object.common.DataId;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

public abstract class RestController<T extends DataId, I extends ScrudBusinessInterface<T>> extends ExceptionHandlerController {

    private I business;

    public RestController(){
        super();
    }

    public RestController(I business) {
        this.business = business;
    }

    @RequestMapping(method = RequestMethod.GET)
    @ResponseStatus(value = HttpStatus.OK)
    @ResponseBody
    public List<T> search() {
        return getBusiness().search();
    }

    @RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(value = HttpStatus.CREATED)
    @ResponseBody
    public void create(@RequestBody T object) {
        getBusiness().create(object);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseStatus(value = HttpStatus.OK)
    @ResponseBody
    public T read(@PathVariable String id) {
        return getBusiness().read(id);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    @ResponseStatus(value = HttpStatus.OK)
    @ResponseBody
    public void update(@PathVariable String id, @RequestBody T object) {
        object.setId(id);
        getBusiness().update(object);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ResponseStatus(value = HttpStatus.OK)
    @ResponseBody
    public void delete(@PathVariable String id) {
        getBusiness().delete(id);
    }

    public I getBusiness() {
        return business;
    }

    public void setBusiness(I business) {
        this.business = business;
    }
}

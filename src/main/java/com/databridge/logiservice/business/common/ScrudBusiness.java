package com.databridge.logiservice.business.common;

import com.databridge.logiservice.dao.common.ScrudDaoInterface;
import com.databridge.logiservice.object.common.DataId;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
public abstract class ScrudBusiness<T extends DataId, I extends ScrudDaoInterface<T>> implements ScrudBusinessInterface<T> {

    private I dao;

    public ScrudBusiness(I dao) {
        this.dao = dao;
    }

    @Override
    public List<T> search() {
        return getDao().search();
    }

    @Override
    public void create(T object) {
        getDao().create(object);
    }

    @Override
    public T read(String id) {
        return getDao().read(id);
    }

    @Override
    public void update(T object) {
        getDao().update(object);
    }

    @Override
    public void delete(String id) {
        getDao().delete(id);
    }

    private I getDao() {
        return dao;
    }
}

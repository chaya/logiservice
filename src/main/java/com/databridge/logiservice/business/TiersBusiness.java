package com.databridge.logiservice.business;

import com.databridge.logiservice.business.common.ScrudBusiness;
import com.databridge.logiservice.dao.ProductDaoInterface;
import com.databridge.logiservice.dao.TiersDaoInterface;
import com.databridge.logiservice.object.Product;
import com.databridge.logiservice.object.Tiers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TiersBusiness extends ScrudBusiness<Tiers, TiersDaoInterface> implements TiersBusinessInterface {

    @Autowired
    public TiersBusiness(TiersDaoInterface dao) {
        super(dao);
    }
}

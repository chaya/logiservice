package com.databridge.logiservice.business;

import com.databridge.logiservice.business.common.ScrudBusinessInterface;
import com.databridge.logiservice.object.Product;

public interface ProductBusinessInterface extends ScrudBusinessInterface<Product> {

}

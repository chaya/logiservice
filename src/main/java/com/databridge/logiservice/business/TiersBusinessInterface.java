package com.databridge.logiservice.business;

import com.databridge.logiservice.business.common.ScrudBusinessInterface;
import com.databridge.logiservice.object.Product;
import com.databridge.logiservice.object.Tiers;

public interface TiersBusinessInterface extends ScrudBusinessInterface<Tiers> {

}

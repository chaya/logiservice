package com.databridge.logiservice.business;

import com.databridge.logiservice.business.common.ScrudBusiness;
import com.databridge.logiservice.dao.ProductDaoInterface;
import com.databridge.logiservice.object.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProductBusiness extends ScrudBusiness<Product, ProductDaoInterface> implements ProductBusinessInterface {

    @Autowired
    public ProductBusiness(ProductDaoInterface dao) {
        super(dao);
    }
}

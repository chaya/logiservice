package com.databridge.logiservice.dao;

import com.databridge.logiservice.dao.common.ScrudDao;
import com.databridge.logiservice.object.Product;
import org.springframework.stereotype.Repository;

@Repository
public class ProductDao extends ScrudDao<Product> implements ProductDaoInterface {

    @Override
    protected Class<Product> getDatabaseObjectClass() {
        return Product.class;
    }
}

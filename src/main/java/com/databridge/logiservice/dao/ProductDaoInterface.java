package com.databridge.logiservice.dao;

import com.databridge.logiservice.dao.common.ScrudDaoInterface;
import com.databridge.logiservice.object.Product;

public interface ProductDaoInterface extends ScrudDaoInterface<Product> {

}

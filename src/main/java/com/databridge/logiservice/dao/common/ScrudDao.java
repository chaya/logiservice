package com.databridge.logiservice.dao.common;

import com.databridge.logiservice.object.common.DataId;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.Table;
import java.util.List;

public abstract class ScrudDao<T extends DataId> implements ScrudDaoInterface<T> {

    private Class<T> clazz;
    private String tableName;

    @Autowired
    private SessionFactory sessionFactory;

    public ScrudDao() {
        clazz = getDatabaseObjectClass();
        Table table = clazz.getAnnotation(Table.class);
        tableName = table.name() != null && !"".equals(table.name()) ? table.name() : clazz.getSimpleName();
    }

    @Override
    public List<T> search() {
        return getSession().createCriteria(clazz).list();
    }

    @Override
    public void create(T object) {
        getSession().save(object);
    }

    @Override
    public T read(String id) {
        return (T) getSession().get(clazz, id);
    }

    @Override
    public void update(T object) {
        getSession().update(object);
    }

    @Override
    public void delete(String id) {
        getSession().delete(getSession().get(clazz, id));
    }

    protected Session getSession() {
        return sessionFactory.getCurrentSession();
    }

    protected abstract Class<T> getDatabaseObjectClass();

    public Class<T> getClazz() {
        return clazz;
    }

    public void setClazz(Class<T> clazz) {
        this.clazz = clazz;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }
}

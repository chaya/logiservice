package com.databridge.logiservice.dao.common;

import com.databridge.logiservice.object.common.DataId;

import java.util.List;

public interface ScrudDaoInterface<T extends DataId> {

    List<T> search();
    void create(T object);
    T read(String id);
    void update(T object);
    void delete(String id);
}

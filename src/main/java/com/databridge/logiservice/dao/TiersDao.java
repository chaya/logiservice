package com.databridge.logiservice.dao;

import com.databridge.logiservice.dao.common.ScrudDao;
import com.databridge.logiservice.object.Product;
import com.databridge.logiservice.object.Tiers;
import org.springframework.stereotype.Repository;

@Repository
public class TiersDao extends ScrudDao<Tiers> implements TiersDaoInterface {

    @Override
    protected Class<Tiers> getDatabaseObjectClass() {
        return Tiers.class;
    }
}

package com.databridge.logiservice.dao;

import com.databridge.logiservice.dao.common.ScrudDaoInterface;
import com.databridge.logiservice.object.Product;
import com.databridge.logiservice.object.Tiers;

public interface TiersDaoInterface extends ScrudDaoInterface<Tiers> {

}

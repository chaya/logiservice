package com.databridge.logiservice.object.common;

import javax.persistence.*;

@MappedSuperclass
public abstract class DataId {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(unique = true, updatable = false, nullable = false) private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}

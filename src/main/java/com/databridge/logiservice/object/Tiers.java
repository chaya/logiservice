package com.databridge.logiservice.object;

import com.databridge.logiservice.object.common.DataId;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(schema = "logifresh", name = "soc_tie")
@AttributeOverride(name = "id", column = @Column(name = "co_tie"))
public class Tiers extends DataId {

    @Column(name = "nom") private String name;
    @Column(name = "co_lan") private String language;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }
}

package com.databridge.logiservice.object;

import com.databridge.logiservice.object.common.DataId;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(schema = "logifresh", name = "soc_art")
@AttributeOverride(name = "id", column = @Column(name = "co_art"))
public class Product extends DataId {

    @Column(name = "li_art") private String lb;
    @Column(name = "li_art_sci") private String scientistLb;
    @Column(name = "co_ean") private Long ean13;
    @Column(name = "pc_vte") private Integer pcb;

    public String getLb() {
        return lb;
    }

    public void setLb(String lb) {
        this.lb = lb;
    }

    public String getScientistLb() {
        return scientistLb;
    }

    public void setScientistLb(String scientistLb) {
        this.scientistLb = scientistLb;
    }

    public Long getEan13() {
        return ean13;
    }

    public void setEan13(Long ean13) {
        this.ean13 = ean13;
    }

    public Integer getPcb() {
        return pcb;
    }

    public void setPcb(Integer pcb) {
        this.pcb = pcb;
    }
}
